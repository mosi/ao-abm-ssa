# MASON SSA Extension

The code artifacts for the paper "Inferring Dependency Graphs for Agent-Based Models using Aspect-Oriented Programming".

If you are looking to integrate this as an extension in your MASON project, an open-source [extension](https://github.com/justinnk/mason-ssa) has been built from this.

## Dependencies

The following software needs to be present on your system:

- Maven 
- MASON (see the next section)
- Java 8
- `python` 3

Tested on the operating systems: `Linux`
(Except for the scripts, it should also work on all other systems satisfying the dependencies, possibly with some minor adjustments)

## Usage

1. Clone the MASON repository from [here](https://github.com/eclab/mason)
2. Inside the root of the mason repository, execute `mvn clean install`
3. Clone this repository
4. Run `mvn clean install`
5. To execute the SIR example with the NRM, run: `./run_example.sh`
6. To reproduce the evaluation results, run: `./run_evaluation.sh` (please refer to the next section for more information)
7. To execute a specific class run: `mvn compile exec:java -Dexec.mainClass="org.justinnk.thesis.demo.<demo_name>.<model_name>"`. Depending on wheter the class uses the NRM, ODM or FMR/LDM simulator, you can append `-P nrm-only`, `-P odm-only` or `-P disable-aspects` at the end of that command to deactivate some aspects. However, it was found that they impose no significant overhead.

## Reproduction

Original Environment:

- Operating System: `Arch Linux, Kernel 5.11.12-arch1-1` (64 bit)
- Processor: `AMD Ryzen 5 1600 @ 3.2 GHz, 6C/12T`
- Memory: `2x 4GB DDR4 2666MHz`

Minimum required:

- Operating System: `Linux`
- Processor: any modern CPU
- Memory: `8GB` usable

Steps:

- Follow steps 1-4 + 6 from the usage section
- The graphs from the thesis should now be found as pdf files in the folder named `/plots/`
- Depending on your exact system configuration, the results of the performance results can differ. However, the trends should be the same
- It is recommended to run the benchmarks in a headless environment with no background porcesses


## Integrate this Extension as a Library

You can find the extension [here](https://github.com/justinnk/mason-ssa).

## Disabling Certain Aspects

The `pom.xml` has three profiles that control the inclusion (weaving) of aspects.
- `disable-aspects`: disables the `NRMDependencySymbiont` and `ODMDependencySymbiont` aspects. This is useful for ubiased testing of the performance of the methods that work without them.
- `nrm-only`: disables the `ODMDependencySymbiont` aspect.
- `odm-only`: disables the `NRMDependencySymbiont` aspect.

## Known issues

- **Problem:** Maven build fails with the message: `invalid flag: --release`
    - **Cause:** Eclipse broke the project for Mason.
    - **Solution:** Clean the project in Eclipse: `Project -> Clean...`
