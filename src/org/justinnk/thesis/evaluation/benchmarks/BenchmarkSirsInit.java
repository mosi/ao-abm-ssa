package org.justinnk.thesis.evaluation.benchmarks;

import org.justinnk.thesis.evaluation.Experiment;
import org.justinnk.thesis.extension.graphs.ErdosRenyiGraphCreator;
import org.justinnk.thesis.extension.graphs.GraphCreator;
import org.justinnk.thesis.extension.ssa.StochasticSimulationAlgorithm;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;

import sim.engine.SimState;

@State(Scope.Thread)
public class BenchmarkSirsInit {
	@Param({"1024"})
	public int numHumans;

	@Param({ "0.48", "0.02", "0.08", "0.16", "0.24", "0.64", "0.32", "0.04" })
	public double density;

	public int currentIteration = 0;
	public SimState model;

	@TearDown(Level.Iteration)
	public void TearDown() {
		model.finish();
	}
	
	protected GraphCreator getGraph() {
		return new ErdosRenyiGraphCreator(Experiment.seed + currentIteration, this.density);
	}
}
