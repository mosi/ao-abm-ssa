package org.justinnk.thesis.evaluation.benchmarks;

import java.util.concurrent.TimeUnit;

import org.justinnk.thesis.demo.ssamason.predatorprey.PredatorPreyModel;
import org.justinnk.thesis.demo.ssamason.sirs.SirsModel;
import org.justinnk.thesis.evaluation.Experiment;
import org.justinnk.thesis.extension.ssa.FirstReactionMethod;
import org.justinnk.thesis.extension.ssa.NextReactionMethod;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Setup;

public class BenchmarkSheepFrm extends BenchmarkSheep {
	
	@Setup(Level.Iteration)
	public void SetUp() {
		// System.out.println("setup model");
		model = new PredatorPreyModel(Experiment.seed + currentIteration, new FirstReactionMethod());
		((PredatorPreyModel) model).setNumSheep(numSheep);
		((PredatorPreyModel) model).setNumWolves(0);
		model.start();
		model.schedule.step(model);
		currentIteration++;
	}

	@BenchmarkMode(Mode.Throughput)
	@OutputTimeUnit(TimeUnit.SECONDS)
	@Benchmark
	public void stepFrm() {
		model.schedule.step(model);
	}
}
