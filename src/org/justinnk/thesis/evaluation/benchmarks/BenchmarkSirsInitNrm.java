package org.justinnk.thesis.evaluation.benchmarks;

import java.util.concurrent.TimeUnit;

import org.justinnk.thesis.demo.ssamason.sirs.SirsModel;
import org.justinnk.thesis.evaluation.Experiment;
import org.justinnk.thesis.extension.SSASimState;
import org.justinnk.thesis.extension.ssa.NextReactionMethod;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

public class BenchmarkSirsInitNrm extends BenchmarkSirsInit {
	
	@Setup(Level.Iteration)
	public void SetUp() {
		model = new SirsModel(Experiment.seed + currentIteration, new NextReactionMethod(), getGraph());
		((SirsModel) model).setNumHumans(this.numHumans);
		model.start();
		currentIteration++;
	}

	@BenchmarkMode(Mode.SingleShotTime)
	@OutputTimeUnit(TimeUnit.SECONDS)
	@Benchmark
	public void initNrm() {
		model.schedule.step(model);
	}
}
