package org.justinnk.thesis.evaluation.benchmarks;

import java.util.concurrent.TimeUnit;

import org.justinnk.thesis.demo.ssamason.predatorprey.PredatorPreyModel;
import org.justinnk.thesis.demo.ssamason.sirs.SirsModel;
import org.justinnk.thesis.evaluation.Experiment;
import org.justinnk.thesis.extension.SSASimState;
import org.justinnk.thesis.extension.ssa.NextReactionMethod;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

public class BenchmarkSheepNrm extends BenchmarkSheep {
	
	public static boolean wasStopped = false;
	
	@Setup(Level.Iteration)
	public void SetUp() {
		model = new PredatorPreyModel(Experiment.seed + currentIteration, new NextReactionMethod());
		((PredatorPreyModel) model).setNumSheep(numSheep);
		((PredatorPreyModel) model).setNumWolves(0);
		model.start();
		model.schedule.step(model);
		currentIteration++;
	}

	@BenchmarkMode(Mode.Throughput)
	@OutputTimeUnit(TimeUnit.SECONDS)
	@Benchmark
	public void stepNrm() {
		do {
			model.schedule.step(model);
		} while (wasStopped);
	}
}
