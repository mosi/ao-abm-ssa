package org.justinnk.thesis.evaluation.benchmarks;

import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.TearDown;

import sim.engine.SimState;

@State(Scope.Thread)
public class BenchmarkSheep {
	
	@Param("10")
	int numSheep;
	
	public int currentIteration = 0;
	public SimState model;

	@TearDown(Level.Iteration)
	public void TearDown() {
		// System.out.println("finish model");
		model.finish();
	}
}
