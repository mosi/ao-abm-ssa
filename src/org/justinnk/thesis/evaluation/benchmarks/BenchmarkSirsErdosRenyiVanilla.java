package org.justinnk.thesis.evaluation.benchmarks;

import java.util.concurrent.TimeUnit;

import org.justinnk.thesis.demo.mason.vanillasirs.SirsModel;
import org.justinnk.thesis.evaluation.Experiment;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Setup;

public class BenchmarkSirsErdosRenyiVanilla extends BenchmarkSirsErdosRenyi {

	public static boolean wasStopped = false;

	@Setup(Level.Iteration)
	public void SetUp() {
		// System.out.println("setup model");
		model = new SirsModel(Experiment.seed + currentIteration, getGraph());
		((SirsModel) model).setNumHumans(this.numHumans);
		model.start();
		currentIteration++;
		model.schedule.step(model);
	}

	@BenchmarkMode(Mode.Throughput)
	@OutputTimeUnit(TimeUnit.SECONDS)
	@Benchmark
	public void stepVanilla() {
		do {
			model.schedule.step(model);
		} while (wasStopped);
	}
}
