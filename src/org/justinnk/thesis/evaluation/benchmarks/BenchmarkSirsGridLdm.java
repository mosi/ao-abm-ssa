package org.justinnk.thesis.evaluation.benchmarks;

import java.util.concurrent.TimeUnit;

import org.justinnk.thesis.demo.ssamason.sirs.SirsModel;
import org.justinnk.thesis.evaluation.Experiment;
import org.justinnk.thesis.extension.SSASimState;
import org.justinnk.thesis.extension.graphs.GridGraphCreator;
import org.justinnk.thesis.extension.ssa.LogarithmicDirectMethod;
import org.justinnk.thesis.extension.ssa.NextReactionMethod;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

/** Benchmark for Logarithmic Direct Method on Erdos-Renyi Graph. */
@State(Scope.Thread)
public class BenchmarkSirsGridLdm extends BenchmarkSirsGrid {

	@Setup(Level.Iteration)
	public void SetUp() {
		// System.out.println("setup model");
		model = new SirsModel(Experiment.seed + currentIteration, new LogarithmicDirectMethod(), new GridGraphCreator());
		((SirsModel) model).setNumHumans(this.gridSize * this.gridSize);
		model.start();
		model.schedule.step(model);
		currentIteration++;
	}

	@BenchmarkMode(Mode.Throughput)
	@OutputTimeUnit(TimeUnit.SECONDS)
	@Benchmark
	public void stepLdm() {
		model.schedule.step(model);
	}
}
