package org.justinnk.thesis.evaluation.correctness;

import java.nio.file.Paths;

import org.justinnk.thesis.demo.ssamason.predatorprey.PredatorPreyModel;
import org.justinnk.thesis.evaluation.CSVWriter;
import org.justinnk.thesis.evaluation.DataFrame;
import org.justinnk.thesis.evaluation.Experiment;
import org.justinnk.thesis.extension.ssa.StochasticSimulationAlgorithm;

import sim.engine.SimState;

public class PredatorPreyCorrectnessExperiment extends Experiment {

	private int numWolf = 0;
	private int numSheep = 0;
	private String algorithmName;
	private StochasticSimulationAlgorithm algorithm;

	public PredatorPreyCorrectnessExperiment(String algorithmName, StochasticSimulationAlgorithm algorithm, int numWolf,
			int numSheep) {
		this.numWolf = numWolf;
		this.numSheep = numSheep;
		this.algorithmName = algorithmName;
		this.algorithm = algorithm;
		System.out.println("Evaluating " + algorithmName + " for correctness on predatorprey.");
	}

	@Override
	protected SimState parameterise() {
		PredatorPreyModel model = new PredatorPreyModel(System.nanoTime(), algorithm);
		model.setNumSheep(this.numSheep);
		model.setNumWolves(this.numWolf);
		return model;
	}

	@Override
	protected void evaluate(DataFrame[] results) {
		for (int i = 0; i < results.length; i++) {
			CSVWriter.writeAndClose(results[i],
				Paths.get(outputPathPrefix + "correctness_data/predatorprey/" + this.algorithmName + "_" + i + ".csv"), true);
		}
		System.out.println("Done.");
	}

	@Override
	protected DataFrame[] execute(SimState model, int replications) {
		double observationStep = 0.25;
		PredatorPreyModelObserver observer = new PredatorPreyModelObserver(observationStep);
		DataFrame[] datas = new DataFrame[replications];
		for (int i = 0; i < replications; i++) {
			System.out.println("Running replication " + i);
			//model.setJob(i);
			model = parameterise();
			model.start();
			datas[i] = new DataFrame();
			observer.data = datas[i];
			/* Schedule the observer to observe every observationStep time units. */
			model.schedule.scheduleRepeating(0, observer, observationStep);
			do {
				if (!model.schedule.step(model)) {
					break;
				}
			} while (((PredatorPreyModel) model).getNumSheep() >= 0 && ((PredatorPreyModel) model).getNumWolves() >= 0
					&& model.schedule.getTime() < 15);
			model.finish();
		}
		/* replace all sum entries with the mean over all replications. */
		//observer.data.replaceAll((k, v) -> (double) v / (double) replications);
		return datas;
	}
}
