package org.justinnk.thesis.demo.ssamason.predatorprey;

public enum Hobby {
	MUSIC, TRAVELLING, SKATEBOARDING
}
