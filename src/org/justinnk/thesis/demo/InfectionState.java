package org.justinnk.thesis.demo;

public enum InfectionState {
	SUSCEPTIBLE, INFECTIOUS, RECOVERED
}
