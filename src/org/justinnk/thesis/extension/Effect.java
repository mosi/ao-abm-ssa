package org.justinnk.thesis.extension;

/** Functional interface for an effect that can be applied. */
public interface Effect {
	/** Apply this effect. */
	void apply();
}
