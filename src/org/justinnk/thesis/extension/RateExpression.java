package org.justinnk.thesis.extension;

/** Functional interface for a rate expression that can be calculated. */
public interface RateExpression {
	/** Calculate the rate. */
	double rate();
}
