import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

DATA_PREFIX = "evaluation/performance_data/sirs/"
PLOT_PREFIX = "plots/performance/"
METHODS = [
    "Vanilla",
    "FirstReactionMethod",
    #"LogarithmicDirectMethod",
    "NextReactionMethod",
    "OptimizedDirectMethod"
]
METHOD_NAMES = [
    "Manual NRM",
    "FRM",
    #"LDM",
    "AO-NRM",
    "AO-DM"
]
CONST_HUMANS = 512
CONST_DENSITY = 0.16
YLABEL = "Actions / Second"

# Plot sirs on grid results
fig, ax = plt.subplots()
for method, method_name in zip(METHODS, METHOD_NAMES):
    data = pd.read_csv(f"{DATA_PREFIX}{method}_grid_jmh.csv")
    data = data.set_index(data["Param: gridSize"]).sort_index()
    ax.errorbar(
        data["Param: gridSize"] * data["Param: gridSize"],
        data["Score"], data["Score Error (99.9%)"],
        label=f"{method_name} (99.9% CI)",
        marker="x", markersize=5, markeredgecolor=(0.5, 0.5,0.5,0.5))
ax.legend()
ax.set_xlabel("Population Size (nH)")
ax.set_ylabel(YLABEL)
ax.set_yscale("log")
ax.set_xscale("log")
ax.set_title("Steady-State Performance of SSAs on the SIRS Model\nusing a Grid-Like Graph")
fig.savefig(f"{PLOT_PREFIX}sirs_grid.png")
fig.savefig(f"{PLOT_PREFIX}sirs_grid.pdf")

# Plot init on erdos renyi results
fig, ax = plt.subplots()
for method, method_name in zip(METHODS, METHOD_NAMES):
    data = pd.read_csv(f"{DATA_PREFIX}{method}_init_jmh.csv")
    data = data.set_index(data["Param: density"]).sort_index()
    ax.errorbar(
        data["Param: density"] * (data["Param: numHumans"] - 1),
        data["Score"], 
        data["Score Error (99.9%)"],
        label=f"{method_name} (99.9% CI)",
        marker="x", markersize=5, markeredgecolor=(0.5, 0.5,0.5,0.5))
ax.legend()
ax.set_xlabel("Average Number of Contacts per Human")
ax.set_ylabel("Initialisation Time [s]")
#ax.set_yscale("log")
#ax.set_xscale("log")
ax.set_title(f"Initialisation Time for dependency-based SSAs on the SIRS model\n(nH = {data['Param: numHumans'].values[0]})")
fig.savefig(f"{PLOT_PREFIX}sirs_init_grid.png")
fig.savefig(f"{PLOT_PREFIX}sirs_init_grid.pdf")

# Plot sirs on erdos-renyi results
fig_const_humans, ax_const_humans = plt.subplots()
fig_const_density, ax_const_density = plt.subplots()
for method, method_name in zip(METHODS, METHOD_NAMES):
    data = pd.read_csv(f"{DATA_PREFIX}{method}_erdos_jmh.csv")

    chumdata = data[np.isclose(data["Param: numHumans"], CONST_HUMANS)]
    cdensdata = data[np.isclose(data["Param: density"], CONST_DENSITY)]

    chumdata = chumdata.set_index(chumdata["Param: density"]).sort_index()
    cdensdata = cdensdata.set_index(cdensdata["Param: numHumans"]).sort_index()

    ax_const_humans.errorbar(
        chumdata["Param: density"] * (CONST_HUMANS - 1),
        chumdata["Score"],
        chumdata["Score Error (99.9%)"],
        label=f"{method_name} (99.9% CI)",
        marker="x", markersize=5, markeredgecolor=(0.5, 0.5,0.5,0.5)
    )

    ax_const_density.errorbar(
        cdensdata["Param: numHumans"],
        cdensdata["Score"],
        cdensdata["Score Error (99.9%)"],
        label=f"{method_name} (99.9% CI)",
        marker="x", markersize=5, markeredgecolor=(0.5, 0.5,0.5,0.5)
    )

ax_const_humans.set_ylabel(YLABEL)
ax_const_humans.set_yscale("log")
#ax_const_humans.set_xscale("log")
ax_const_humans.legend()
ax_const_humans.set_xlabel("Average Number of Contacts per Human")
ax_const_humans.set_title(f"Steady-State Performance of SSA on the SIRS Model initialised\nwith an Erdős-Rényi Graph (nH={CONST_HUMANS})")

ax_const_density.set_ylabel(YLABEL)
ax_const_density.set_xlabel("Number of Agents")
ax_const_density.set_yscale("log")
ax_const_density.set_xscale("log")
ax_const_density.legend()
ax_const_density.set_title(f"Sensitivity of Performance dependening on\n\
    the Number of Agents (Density = {CONST_DENSITY})")

fig_const_humans.savefig(f"{PLOT_PREFIX}sirs_const_humans.png")
fig_const_humans.savefig(f"{PLOT_PREFIX}sirs_const_humans.pdf")
fig_const_density.savefig(f"{PLOT_PREFIX}sirs_const_density.png")
fig_const_density.savefig(f"{PLOT_PREFIX}sirs_const_density.pdf")

# aop overhead
fig, ax = plt.subplots()

frm = pd.read_csv(f"{DATA_PREFIX}FirstReactionMethod_grid_jmh.csv")
frm = frm.set_index(frm["Param: gridSize"]).sort_index()
frm_aop = pd.read_csv(f"{DATA_PREFIX}FirstReactionMethod_grid_overhead_jmh.csv")
frm_aop = frm_aop.set_index(frm_aop["Param: gridSize"]).sort_index()

ax.errorbar(
        frm["Param: gridSize"] * frm["Param: gridSize"],
        frm["Score"], frm["Score Error (99.9%)"],
        label=f"FRM (99.9% CI)",
        marker="x", markersize=5, markeredgecolor=(0.5, 0.5,0.5,0.5))

ax.errorbar(
        frm_aop["Param: gridSize"] * frm_aop["Param: gridSize"],
        frm_aop["Score"], frm_aop["Score Error (99.9%)"],
        label=f"FRM + NRM Aspect (99.9% CI)",
        marker="x", markersize=5, markeredgecolor=(0.5, 0.5,0.5,0.5))
    
ax.legend()
ax.set_xlabel("Population Size")
ax.set_ylabel(YLABEL)
ax.set_yscale("log")
#ax.set_xscale("log")
ax.set_title("Influence of Weaving the AO-NRM-Aspect")
fig.savefig(f"{PLOT_PREFIX}sirs_overhead_grid.png")
fig.savefig(f"{PLOT_PREFIX}sirs_overhead_grid.pdf")

