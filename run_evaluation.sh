#!/bin/sh

mvn clean
mvn compile exec:java -Dexec.mainClass="org.justinnk.thesis.evaluation.Evaluator" -Dexec.args="nrm" -P nrm-only
mvn clean
mvn compile exec:java -Dexec.mainClass="org.justinnk.thesis.evaluation.Evaluator" -Dexec.args="odm" -P odm-only
mvn clean
mvn compile exec:java -Dexec.mainClass="org.justinnk.thesis.evaluation.Evaluator" -Dexec.args="vanilla" -P disable-aspects
mvn clean
mvn compile exec:java -Dexec.mainClass="org.justinnk.thesis.evaluation.Evaluator" -Dexec.args="frm" -P disable-aspects
mvn clean
#mvn compile exec:java -Dexec.mainClass="org.justinnk.thesis.evaluation.Evaluator" -Dexec.args="ldm" -P disable-aspects
#mvn clean
mvn compile exec:java -Dexec.mainClass="org.justinnk.thesis.evaluation.Evaluator" -Dexec.args="frm_overhead" -P nrm-only
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
python plot_correctness.py
python plot_performance.py
deactivate
