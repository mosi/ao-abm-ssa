import pandas as pd
import numpy as np
from matplotlib.transforms import Affine2D
import matplotlib.pyplot as plt

#
# Helper Functions
#

FOLDER_PREFIX = "plots/correctness/"
STEP_SIZE = 1 / 0.25

def ci(sem):
    return sem * 3.29

#
# Plotting 
#

# SIR model
fig, ax = plt.subplots()
for method, color, name in zip(
    [
        "FirstReactionMethod", "LogarithmicDirectMethod",
        "NextReactionMethod", "OptimizedDirectMethod",
    ], 
    [
        "#ff7f0e", "#2ca02c", "#d62728", "#9467bd"
    ],
    [
        "FRM", "LDM", "AO-NRM", "AO-DM"
    ]):
    data = []
    for i in range(0,2000):
        data.append(pd.read_csv(f"evaluation/correctness_data/sir/{method}_{i}.csv", index_col="index").sort_index())
    mean = pd.concat(data).groupby(level=0).mean()
    sem = pd.concat(data).groupby(level=0).sem()
    inf_ci = ci(sem.infected)
    sus_ci = ci(sem.susceptible)
    rec_ci = ci(sem.recovered)
    _,_,bars = ax.errorbar(mean.index / STEP_SIZE, mean.infected, inf_ci, label=f"{name} (99.9% CI)", color=color, linestyle="solid", errorevery=1)
    [bar.set_alpha(0.05) for bar in bars]
    _,_,bars = ax.errorbar(mean.index / STEP_SIZE, mean.susceptible, sus_ci, label=None, color=color, linestyle="dotted", errorevery=1)
    [bar.set_alpha(0.05) for bar in bars]
    _,_,bars = ax.errorbar(mean.index / STEP_SIZE, mean.recovered, rec_ci, label=None, color=color, linestyle="dashed", errorevery=1)
    [bar.set_alpha(0.05) for bar in bars]
ax.legend()
ax.set_title("Comparison of SIR Simulation results for different SSAs")
ax.set_ylabel("Number of Agents")
ax.set_xlabel("Simulation Time")
fig.tight_layout()
fig.savefig(f"{FOLDER_PREFIX}sir_correctness.png")
fig.savefig(f"{FOLDER_PREFIX}sir_correctness.pdf")

# SIRS model
fig, ax = plt.subplots()
for method, color, name in zip(
    [
        "vanilla", "FirstReactionMethod", #"LogarithmicDirectMethod",
        "NextReactionMethod", "OptimizedDirectMethod",
    ], 
    [
        "#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd"
    ],
    [
        "Manual NRM", "FRM", #"LDM",
        "AO-NRM", "AO-DM"
    ]):
    data = []
    for i in range(0,800):
        data.append(pd.read_csv(f"evaluation/correctness_data/sirs/{method}_{i}.csv", index_col="index").sort_index())
    mean = pd.concat(data).groupby(level=0).mean()
    sem = pd.concat(data).groupby(level=0).sem()
    #inf_ci = 0#ci(sem.infected)
    #sus_ci = 0#ci(sem.susceptible)
    #rec_ci = 0#ci(sem.recovered)
    #if method == "FirstReactionMethod":
    inf_ci = ci(sem.infected)
    sus_ci = ci(sem.susceptible)
    rec_ci = ci(sem.recovered)
    _,_,bars = ax.errorbar(mean.index / STEP_SIZE, mean.infected, inf_ci, label=f"{name} (99.9% CI)", color=color, linestyle="solid", errorevery=1)
    [bar.set_alpha(0.04) for bar in bars]
    _,_,bars = ax.errorbar(mean.index / STEP_SIZE, mean.susceptible, sus_ci, label=None, color=color, linestyle="dotted", errorevery=1)
    [bar.set_alpha(0.04) for bar in bars]
    _,_,bars = ax.errorbar(mean.index / STEP_SIZE, mean.recovered, rec_ci, label=None, color=color, linestyle="dashed", errorevery=1)
    [bar.set_alpha(0.04) for bar in bars]
ax.legend()
ax.set_title("Comparison of SIRS Simulation results for different SSAs")
ax.set_ylabel("Number of Agents")
ax.set_xlabel("Simulation Time")
fig.tight_layout()
fig.savefig(f"{FOLDER_PREFIX}sirs_correctness.png")
fig.savefig(f"{FOLDER_PREFIX}sirs_correctness.pdf")

# Predator Prey model
fig, ax = plt.subplots()
for method, color, name in zip(
    [
        "FirstReactionMethod", "LogarithmicDirectMethod",
        "NextReactionMethod", "OptimizedDirectMethod",
    ], 
    [
        "#ff7f0e", "#2ca02c", "#d62728", "#9467bd"
    ],
    [
        "FRM", "LDM", "AO-NRM", "AO-DM"
    ]):
    data = []
    for i in range(0,800):
        data.append(pd.read_csv(f"evaluation/correctness_data/predatorprey/{method}_{i}.csv", index_col="index").sort_index())
    mean = pd.concat(data).groupby(level=0).mean()
    sem = pd.concat(data).groupby(level=0).sem()
    wolf_ci = ci(sem.wolf)
    sheep_ci = ci(sem.sheep)
    ax.errorbar(mean.index / STEP_SIZE, mean.sheep, sheep_ci, label=f"{name} (99.9% CI)", color=color, linestyle="solid", errorevery=1)
    ax.errorbar(mean.index / STEP_SIZE, mean.wolf, wolf_ci, label=None, color=color, linestyle="dotted", errorevery=1)
ax.legend()
ax.set_title("Comparison of Predator-Prey Simulation results for different SSAs")
ax.set_ylabel("Number of Agents")
ax.set_xlabel("Simulation Time")
fig.tight_layout()
fig.savefig(f"{FOLDER_PREFIX}predatorprey_correctness.png")
fig.savefig(f"{FOLDER_PREFIX}predatorprey_correctness.pdf")


fig, ax = plt.subplots()
width = 0.1
w = 0
for method, color, name in zip(
    [
        "Vanilla", "FirstReactionMethod", "LogarithmicDirectMethod",
        "NextReactionMethod", "OptimizedDirectMethod",
    ], 
    [
        "#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd"
    ],
    [
        "Manual NRM", "FRM", "LDM", "AO-NRM", "AO-DM"
    ]):
    data = pd.read_csv(f"evaluation/performance_data/sirs/{method}_algo.csv", index_col="index")
    cols = data.columns[:-1].values
    for i, col in enumerate(cols):
        if i == 0:
            ax.bar(i + w * width, data[col].values[0], width=width, color=color, label=name)
        else:
            ax.bar(i + w * width, data[col].values[0], width=width, color=color)
    w += 1
ax.legend()
ax.set_title("Absolute Number of Common SSA Calls")
ax.set_yscale("log")
ax.set_xticks(np.array([0,1,2]) + width)
ax.set_xticklabels(cols)
ax.set_xlabel("SSA Call")
ax.set_ylabel("Absolute Number of Calls")
fig.savefig(f"{FOLDER_PREFIX}sirs_algo.png")
fig.savefig(f"{FOLDER_PREFIX}sirs_algo.pdf")

"""
# SIR end histogram
fig, ax = plt.subplots(ncols=6,nrows=2,sharex=True,sharey=True,figsize=(10,10))
l = 0
beg = 0
end = 3
for method, color in zip(
    [
        "FirstReactionMethod", "LogarithmicDirectMethod",
        "NextReactionMethod", "OptimizedDirectMethod",
    ], 
    [
        "red", "green", "blue", "orange", "purple"
    ]):
    if method == "LogarithmicDirectMethod":
        beg = end
        end = 6
    if method == "NextReactionMethod":
        beg = 0
        end = 3
        l = 1
    if method == "OptimizedDirectMethod":
        beg = end
        end = 6
    data = []
    for i in range(0,2000):
        data.append(pd.read_csv(f"evaluation/correctness_data/sir/{method}_{i}.csv", index_col="index").sort_index())
    lasts = pd.concat(data).groupby(level=0)[["susceptible","infected","recovered"]].last().hist(ax=ax[l][beg:end])
fig.savefig(f"{FOLDER_PREFIX}sir_end_hist.png")
"""

"""
# SIRS end histogram
fig, ax = plt.subplots(ncols=6,nrows=2,sharex=True,sharey=True,figsize=(10,10))
l = 0
beg = 0
end = 3
for method, color in zip(
    [
        "FirstReactionMethod", "LogarithmicDirectMethod",
        "NextReactionMethod", "OptimizedDirectMethod",
    ], 
    [
        "#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd"
    ]):
    if method == "LogarithmicDirectMethod":
        beg = end
        end = 6
    if method == "NextReactionMethod":
        beg = 0
        end = 3
        l = 1
    if method == "OptimizedDirectMethod":
        beg = end
        end = 6
    data = []
    for i in range(0,800):
        data.append(pd.read_csv(f"evaluation/correctness_data/sirs/{method}_{i}.csv", index_col="index").sort_index())
    lasts = pd.concat(data).groupby(level=0)[["susceptible","infected","recovered"]].last().hist(ax=ax[l][beg:end])
fig.savefig(f"{FOLDER_PREFIX}sirs_end_hist.png")
"""